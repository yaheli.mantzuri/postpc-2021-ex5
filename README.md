I pledge the highest level of ethical principles in support of academic excellence.  I ensure that
all of my work reflects my own abilities and not those of someone else.


We didn't define any UX flow to let users edit a descrption on an existing TODO item.
Which UX flow will you define?
In your response notice the following:
1. how easy is it for users to figure out this flow in their first usage? (for example, a glowing
 button is more discoverable then a swipe-left gesture)
2. how hard to implement will your solution be?
3. how consistent is this flow with regular "edit" flows in the Android world?


I will use the familiar Android edit button to make it easier for users. Clicking this button
will put all the tasks in edit mode and the user will choose which task to edit. This thing
will not be  difficult I will use Listener.