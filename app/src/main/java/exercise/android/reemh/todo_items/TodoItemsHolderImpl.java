package exercise.android.reemh.todo_items;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;


public class TodoItemsHolderImpl implements TodoItemsHolder{
  public List<TodoItem> listTodoItem = new ArrayList<>();
  public static  int counter = 0;
  private final Context context;
  public int todoIDGenerator = 0;
  private final SharedPreferences sp;
  int new_counter = 0;
  int new_ID = 0;


  public TodoItemsHolderImpl(Context context){
    this.context = context;

    this.sp = context.getSharedPreferences("local_db_td", Context.MODE_PRIVATE);
    initializeFromSp();
    Collections.sort(listTodoItem, new sortToDoItems());
  }

  private void initializeFromSp(){
    Set<String> keys = sp.getAll().keySet();

    for (String key :keys){
      String toDoSave = sp.getString(key, null);

      TodoItem todoItem = getToDoFromString(toDoSave);
      if (todoItem != null){
        listTodoItem.add(todoItem);
      }
//      todoIDGenerator = listTodoItem.size() + 1;
      counter = new_counter + 1 ;
      todoIDGenerator =new_ID + 1;


    }
  }

  public String getStringfromToDo(TodoItem todoItem){
    return todoItem.description + "#" + todoItem.status + "#" + todoItem.counter + "#" + todoItem.toDoId + "#" + todoItem.lastModifiedDate + "#" + todoItem.lastModifiedHour;
  }

  public TodoItem getToDoFromString(String str){
    if (str == null){
      return null;
    }

    String[] arrOfStr1 = str.split("#", 6);
    TodoItem todoItem = new TodoItem();
    todoItem.description = arrOfStr1[0];
    todoItem.status = arrOfStr1[1];
    todoItem.counter = Integer.parseInt(arrOfStr1[2]);
    if (new_counter < todoItem.counter){
      new_counter = todoItem.counter;
    }
    todoItem.toDoId = Integer.parseInt(arrOfStr1[3]);
    if (new_ID < todoItem.toDoId){
      new_ID = todoItem.toDoId;
    }
    todoItem.lastModifiedDate =  arrOfStr1[4];
    todoItem.lastModifiedHour = arrOfStr1[5];

    return todoItem;
  }




  /** Get a copy of the current items list */
  @Override
  public ArrayList<TodoItem> getCurrentItems() {
    ArrayList<TodoItem> copy = new ArrayList<>();
    copy.addAll(listTodoItem);
    return copy;
  }


  /**
   * Creates a new TodoItem and adds it to the list, with the @param description
   * and status=IN-PROGRESS
   * Subsequent calls to [getCurrentItems()] should have this new TodoItem in the list
   */
  @RequiresApi(api = Build.VERSION_CODES.O)
  @Override
  public void addNewInProgressItem(String description) {
    TodoItem todoItem = new TodoItem();
    todoItem.description = description;
    todoItem.status = "IN-PROGRESS";
    todoItem.counter = counter;
    counter += 1;

    todoItem.toDoId = todoIDGenerator;
    todoIDGenerator++;

    listTodoItem.add(0,todoItem);
    Collections.sort(listTodoItem, new sortToDoItems());
    sendBroadcastTdChanged();

    SharedPreferences.Editor editor = sp.edit();
    editor.putString(Integer.toString(todoItem.toDoId), getStringfromToDo(todoItem));
    editor.apply();
  }

  /** mark the @param item as DONE */
  @RequiresApi(api = Build.VERSION_CODES.O)
  @Override
  public void markItemDone(TodoItem item) {
    item.status = "DONE";
    Collections.sort(listTodoItem, new sortToDoItems());

    SharedPreferences.Editor editor = sp.edit();
    editor.putString(Integer.toString(item.toDoId), getStringfromToDo(item));
    editor.apply();
  }

  /** mark the @param item as IN-PROGRESS */
  @RequiresApi(api = Build.VERSION_CODES.O)
  @Override
  public void markItemInProgress(TodoItem item) {
    item.status = "IN-PROGRESS";
    Collections.sort(listTodoItem, new sortToDoItems());

    SharedPreferences.Editor editor = sp.edit();
    editor.putString(Integer.toString(item.toDoId), getStringfromToDo(item));
    editor.apply();
  }

  /** delete the @param item */
  @Override
  public void deleteItem(TodoItem item) {
    listTodoItem.remove(item);
    Collections.sort(listTodoItem, new sortToDoItems());

    sendBroadcastTdChanged();

    SharedPreferences.Editor editor = sp.edit();
    editor.remove(Integer.toString(item.toDoId));
    editor.apply();
  }


  public Serializable saveState() {
      TodoItemState state = new TodoItemState();
      state.listTodoItem = listTodoItem;
      state.counter = counter;
      state.todoIDGenerator = todoIDGenerator;
      return state;
  }


  public void loadState(Serializable prevState) {
      if (!(prevState instanceof TodoItemState)) {
          return; // ignore
      }
      TodoItemState casted = (TodoItemState) prevState;
      listTodoItem = casted.listTodoItem;
      counter = casted.counter;
      todoIDGenerator = casted.todoIDGenerator;
  }

  private static class TodoItemState implements Serializable {
      List<TodoItem> listTodoItem;
      int counter;
      int todoIDGenerator;
  }



  public @Nullable TodoItem getById(int todoID){
//    if (todoID == null){
//      return null;
//    }
    for (TodoItem todoItem : listTodoItem ){
      if ((todoItem.toDoId) == todoID){
        return todoItem;
      }
    }
    return null;
  }

  public void editItemDescription(int todoID, String description) {
    TodoItem editItem = getById(todoID);
    if (editItem == null){
      return;
    }

    TodoItem todoItem = new TodoItem();
    todoItem.description = description;
    todoItem.status = editItem.status;
    todoItem.counter = editItem.counter;
    todoItem.toDoId = editItem.toDoId;

    listTodoItem.remove(editItem);


    listTodoItem.add(0,todoItem);
    Collections.sort(listTodoItem, new sortToDoItems());


    Date forDate = new Date();
    SimpleDateFormat ft = new SimpleDateFormat ("MM/dd/yyyy", Locale.ENGLISH);
    todoItem.lastModifiedDate =  ft.format(forDate);
    SimpleDateFormat ft2 = new SimpleDateFormat ("hh:mm:ss");
    todoItem.lastModifiedHour =  ft2.format(forDate);



    sendBroadcastTdChanged();

    SharedPreferences.Editor editor = sp.edit();
    editor.putString(Integer.toString(todoItem.toDoId), getStringfromToDo(todoItem));
    editor.apply();

  }



  public void editItemStatus(int todoID, String status) {
    TodoItem editItem = getById(todoID);
    if (editItem == null){
      return;
    }

    TodoItem todoItem = new TodoItem();
    todoItem.description = editItem.description;
    todoItem.status = status;
    todoItem.counter = editItem.counter;
    todoItem.toDoId = editItem.toDoId;

    listTodoItem.remove(editItem);


    listTodoItem.add(0,todoItem);
    Collections.sort(listTodoItem, new sortToDoItems());

    Date forDate = new Date();
    SimpleDateFormat ft = new SimpleDateFormat ("MM/dd/yyyy", Locale.ENGLISH);
    todoItem.lastModifiedDate =  ft.format(forDate);
    SimpleDateFormat ft2 = new SimpleDateFormat ("hh:mm:ss");
    todoItem.lastModifiedHour =  ft2.format(forDate);



    sendBroadcastTdChanged();

    SharedPreferences.Editor editor = sp.edit();
    editor.putString(Integer.toString(todoItem.toDoId), getStringfromToDo(todoItem));
    editor.apply();

  }

  public void sendBroadcastTdChanged(){
    Intent broadcast = new Intent("td_changed");

    broadcast.putExtra("new_list", getCurrentItems());
    broadcast.putExtra("counter", counter);
    broadcast.putExtra("todoIDGenerator", todoIDGenerator);

//    context.sendBroadcast(broadcast);

    LocalBroadcastManager.getInstance(context).sendBroadcast(broadcast);
  }


}
