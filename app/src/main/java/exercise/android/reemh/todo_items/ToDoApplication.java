package exercise.android.reemh.todo_items;

import android.os.Bundle;
import android.app.Application;

public class ToDoApplication extends Application {

    private TodoItemsHolderImpl todoItemsHolder;

    public TodoItemsHolderImpl getTodoItemsHolder() {
        return todoItemsHolder;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        todoItemsHolder = new TodoItemsHolderImpl(this);
    }

    private  static ToDoApplication instance = null;

    public static ToDoApplication getInstance() {
        return instance;
    }


}
