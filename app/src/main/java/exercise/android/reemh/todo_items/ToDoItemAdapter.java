package exercise.android.reemh.todo_items;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;



public class ToDoItemAdapter extends RecyclerView.Adapter  {

    ArrayList<TodoItem> _listTodoItem = new ArrayList<>();
    TodoItemsHolder todoItemsHolder;

    public ToDoItemAdapter(TodoItemsHolder TodoItemsHolder){
        this.todoItemsHolder = TodoItemsHolder;
    }

    public void setItemToDo(List<TodoItem> list_items){
        _listTodoItem.clear();
        _listTodoItem.addAll(list_items);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_todo_item, parent, false);
        return new holderItems(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        TodoItem todoItem = _listTodoItem.get(position);
        holderItems holder1 = (holderItems) holder;
        holder1.description.setText(todoItem.description);

        if (todoItem.status.equals("DONE")){
            holder1.status.setChecked(true);
            holder1.description.setPaintFlags(holder1.description.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        else {
            holder1.status.setChecked(false);
            holder1.description.setPaintFlags(holder1.description.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }


        holder1.status.setOnClickListener(v -> {
            if (todoItem.status.equals("DONE")){
                todoItemsHolder.markItemInProgress(todoItem);
                holder1.status.setChecked(false);
                holder1.description.setPaintFlags(holder1.description.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                setItemToDo(todoItemsHolder.getCurrentItems());
            } else {
                todoItemsHolder.markItemDone(todoItem);
                holder1.description.setPaintFlags(holder1.description.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                holder1.status.setChecked(true);
                setItemToDo(todoItemsHolder.getCurrentItems());
            }
            notifyDataSetChanged();
        });

        holder1.description.setOnLongClickListener(v -> {
            todoItemsHolder.deleteItem(todoItem);
            setItemToDo(todoItemsHolder.getCurrentItems());
            notifyDataSetChanged();
            return false;
        });

        holder1.description.setOnClickListener(v -> {
            Intent intent = new Intent(holder1.itemView.getContext(), EditToDoActivity.class);
            intent.putExtra("date", todoItem.date);
            intent.putExtra("time", todoItem.time);
            intent.putExtra("description", todoItem.description);
            intent.putExtra("status", todoItem.status);
            intent.putExtra("saveState", todoItemsHolder.saveState());
            intent.putExtra("ID", todoItem.toDoId);
            intent.putExtra("lastModifiedDate", todoItem.lastModifiedDate);
            intent.putExtra("lastModifiedHour", todoItem.lastModifiedHour);
            holder1.itemView.getContext().startActivity(intent);

            notifyDataSetChanged();
        });


//        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return _listTodoItem.size();
    }



    class holderItems extends RecyclerView.ViewHolder{
          TextView description;
          CheckBox status;


      public holderItems(View view) {
        super(view);
        description = view.findViewById(R.id.description);
        status = view.findViewById(R.id.checkBox);
      }
    }

}
