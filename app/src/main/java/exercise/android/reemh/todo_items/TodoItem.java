package exercise.android.reemh.todo_items;

import org.mockito.internal.matchers.Null;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import java.util.Date;

import java.util.*;
import java.text.*;

public class TodoItem implements Serializable {
    String description;
    String status;
    int counter;
    int toDoId;
    Date dNow = new Date( );

    SimpleDateFormat ft =
            new SimpleDateFormat ("MM/dd/yyyy", Locale.ENGLISH);
    String date =  ft.format(dNow);
    SimpleDateFormat ft2 = new SimpleDateFormat ("hh:mm:ss");
    String time =  ft2.format(dNow);

    String lastModifiedDate = ft.format(dNow);
    String lastModifiedHour = ft2.format(dNow);


    public TodoItem() {

    }

}


class sortToDoItems implements Comparator<TodoItem> {

    @Override
    public int compare(TodoItem o1, TodoItem o2) {
        if (o1.status.equals("DONE") && o2.status.equals("DONE")){
            return 0;
        }
        if (o1.status.equals("DONE")){
            return 1;
        }
        if (o2.status.equals("DONE")){
            return -1;
        }
        else {
            return (o2.counter - o1.counter);
        }
    }
}
