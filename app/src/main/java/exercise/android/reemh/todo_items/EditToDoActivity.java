package exercise.android.reemh.todo_items;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class EditToDoActivity extends AppCompatActivity {


    public TodoItemsHolderImpl todoItemsHolder = null;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.edit_todo_item);


        if (todoItemsHolder == null){
            todoItemsHolder = ToDoApplication.getInstance().getTodoItemsHolder();
        }

        Intent intentOpenedMe = getIntent();

        boolean check1 =intentOpenedMe.hasExtra("date");
        boolean check2 =intentOpenedMe.hasExtra("time");
        boolean check3 =intentOpenedMe.hasExtra("description");
        boolean check4 =intentOpenedMe.hasExtra("status");
        boolean check5 =intentOpenedMe.hasExtra("ID");
        boolean check6 =intentOpenedMe.hasExtra("lastModifiedDate");
        boolean check7 =intentOpenedMe.hasExtra("lastModifiedHour");

        if (check1 && check2 && check3 && check4 && check5 && check6 && check7){
            String date = intentOpenedMe.getStringExtra("date");
            String time = intentOpenedMe.getStringExtra("time");
            String description = intentOpenedMe.getStringExtra("description");
            String status = intentOpenedMe.getStringExtra("status");


            int ID = intentOpenedMe.getIntExtra("ID", 0);

            String lastModifiedDate = intentOpenedMe.getStringExtra("lastModifiedDate");
            String lastModifiedHour = intentOpenedMe.getStringExtra("lastModifiedHour");




            TextView textView1 = findViewById(R.id.status);


            Date dNow = new Date( );
            SimpleDateFormat ft = new SimpleDateFormat ("MM/dd/yyyy");
            String newDate =  ft.format(dNow);

            SimpleDateFormat ft2 = new SimpleDateFormat ("hh:mm:ss");
            String newTime =  ft2.format(dNow);

            Date firstDate = null;
            try {
                firstDate = ft.parse(lastModifiedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date secondDate = null;
            try {
                secondDate = ft.parse(newDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
            long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);


            if (diff > 0){
                textView1.setText(date + "\n" + "modified: " + lastModifiedDate + " at " + lastModifiedHour+ "\n" + description + "\n" + status);
            }
            else {
                String[] arrOfStr1 = lastModifiedHour.split(":", 3);

                String[] arrOfStr2 = newTime.split(":", 3);

                if (Integer.parseInt(arrOfStr2[0]) - Integer.parseInt(arrOfStr1[0]) >= 1){
                    textView1.setText(date + "\n" + "modified: "  + "Today at " + lastModifiedHour+ "\n" + description + "\n" + status);
                } else {
                    long diff2 = (Integer.parseInt(arrOfStr2[1]) - Integer.parseInt(arrOfStr1[1]));
                    textView1.setText(date + "\n" + "modified: " + diff2 + " minutes ago"+ "\n" + description + "\n" + status);
                }

            }



            CheckBox checkBox = findViewById(R.id.checkBox2);

            checkBox.setChecked(status.equals("DONE"));



            checkBox.setOnClickListener(v -> {
                if (todoItemsHolder.getById(ID).status.equals("DONE")){
                    checkBox.setChecked(false);
                    todoItemsHolder.editItemStatus(ID, "IN-PROGRESS");
                } else {
                    checkBox.setChecked(true);
                    todoItemsHolder.editItemStatus(ID, "DONE");

                }
            });






            EditText editTextInsertTask = findViewById(R.id.editTextInsertTask);

            editTextInsertTask.setText("");
            editTextInsertTask.setEnabled(true);

            editTextInsertTask.addTextChangedListener(new TextWatcher() {
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                public void onTextChanged(CharSequence s, int start, int before, int count) { }
                public void afterTextChanged(Editable s) {
                    String newText = editTextInsertTask.getText().toString();
                    todoItemsHolder.editItemDescription(ID, newText);
                }
            });
        }






    }


}
